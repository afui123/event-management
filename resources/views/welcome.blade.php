@extends('layouts.front')

@section('meta')
    <title>Moltran - Responsive Admin Dashboard Template</title>
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
@endsection

@section('style')
@endsection

@section('content')

<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="pull-left page-title">Dashboard</h4>
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Moltran</a></li>
                    <li><a href="#">Pages</a></li>
                    <li class="active">Blank Page</li>
                </ol>
            </div>
        </div>

        <!-- Pls Remove -->
        <div style="height:600px;"></div>


    </div> <!-- container -->
               
</div> <!-- content -->
@endsection

@section('script')
@endsection