<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ url('admin/images/favicon_1.ico')}}">

        @yield('meta')


        <link href="{{ url('admin/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{ url('admin/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
        <link href="{{ url('admin/assets/ionicon/css/ionicons.min.css')}}" rel="stylesheet" />
        <link href="{{ url('admin/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
        <link href="{{ url('admin/css/animate.css')}}" rel="stylesheet" />
        <link href="{{ url('admin/css/waves-effect.css')}}" rel="stylesheet">
        <!-- Custom Files -->
        <link href="{{ url('admin/css/helper.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ url('admin/css/style.css')}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('style')

        <script src="{{ url('admin/js/modernizr.min.js')}}"></script>
        
    </head>



    <body class="fixed-left">
        
        <!-- Begin page -->
        <div id="wrapper">
        
            @include('include.header-dashboard')
            @include('include.menu-dashboard')
            <div class="content-page">
                @yield('content')

                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>

        </div>
        <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('admin/js/jquery.min.js')}}"></script>
        <script src="{{ url('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{ url('admin/js/waves.js')}}"></script>
        <script src="{{ url('admin/js/wow.min.js')}}"></script>
        <script src="{{ url('admin/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
        <script src="{{ url('admin/js/jquery.scrollTo.min.js')}}"></script>
        <script src="{{ url('admin/assets/jquery-detectmobile/detect.js')}}"></script>
        <script src="{{ url('admin/assets/fastclick/fastclick.js')}}"></script>
        <script src="{{ url('admin/assets/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
        <script src="{{ url('admin/assets/jquery-blockui/jquery.blockUI.js')}}"></script>


        <!-- CUSTOM JS -->
        <script src="{{ url('admin/js/jquery.app.js')}}"></script>
        @yield('script')
    
    </body>
</html>