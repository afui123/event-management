<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  @yield('meta')
  <title>eBusiness Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ url('front/img/favicon.png')}}" rel="icon">
  <link href="{{ url('front/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
  <link href="{{ url('front/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/nivo-slider/css/nivo-slider.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/owlcarousel/owl.transitions.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{ url('front/lib/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{ url('front/css/nivo-slider-theme.css')}}" rel="stylesheet">
  <link href="{{ url('front/css/style.css')}}" rel="stylesheet">
  <link href="{{ url('front/css/responsive.css')}}" rel="stylesheet">
  @yield('style')
</head>

<body data-spy="scroll" data-target="#navbar-example">

  <div id="preloader"></div>

  <header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">

            <!-- Navigation -->
            <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <div class="col-md-4 col-sm-4">
                <!-- Brand -->
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                <a class="navbar-brand page-scroll sticky-logo" href="#">
                  <h1><span>e</span>Business <button type="button" class="btn btn-info btn-sm"><b>LOKET FOR BUSSINESS</b></button></h1>
                </a>          
                </div>
   
    
      
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                <ul class="nav navbar-nav navbar-right">
                    
                    <li>
                    <a class="page-scroll" href="#">CARI EVENT</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="#">MASUK</a>
                  </li>
                  <li>
                    <a class="page-scroll" href="#">MENU</a>
                  </li>
                </ul>
              </div>
              <!-- navbar-collapse -->
            </nav>
            <!-- END: Navigation -->
          </div>
        </div>
      </div>
    </div>
    <!-- header-area end -->
  </header>
  <!-- header end -->

  <!-- Start Slider Area -->
  <div id="home" class="slider-area" >
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <img  height="970" src="{{ url('front/img/slider/slider1.jpg')}}" alt="" title="#slider-direction-1" />
        <img src="{{ url('front/img/slider/slider2.jpg')}}" alt="" title="#slider-direction-2" />
        <img src="{{ url('front/img/slider/slider3.jpg')}}" alt="" title="#slider-direction-3" />
      </div>

      <!-- direction 1 -->
      <div id="slider-direction-1" class="slider-direction slider-one">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
                <!-- layer 1 -->
                <div class="layer-1-1 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
                  <h2 class="title1">Mau Bikin</h2>
                </div>
                <!-- layer 2 -->
                <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">Arisan,Kajian,Reunian</h1>
                </div>
                <!-- layer 3 -->
                <div class="layer-1-3  wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <a class="ready-btn right-btn page-scroll" href="#">Buat Eventmu Sekarang</a>
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- direction 2 -->
      <div id="slider-direction-2" class="slider-direction slider-two">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content text-center">
                <!-- layer 1 -->
                <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <h2 class="title1">Mau Bikin </h2>
                </div>
                <!-- layer 2 -->
                <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">Open Trip,Garage Sale,Nobar</h1>
                </div>
                <!-- layer 3 -->
                <div class="layer-1-3  wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <a class="ready-btn right-btn page-scroll " >Buat Eventmu Sekarang</a>
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- direction 3 -->
      <div id="slider-direction-3" class="slider-direction slider-two">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
                <!-- layer 1 -->
                <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <h2 class="title1">Apapun Eventmu</h2>
                </div>
                <!-- layer 2 -->
                <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">#JadiinAja</h1>
                </div>
                <!-- layer 3 -->
                <div class="layer-1-3 wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <a class="ready-btn right-btn page-scroll" href="#">Buat Eventmu Sekarang</a>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Slider Area -->

  
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ url('front/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{ url('front/lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ url('front/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{ url('front/lib/venobox/venobox.min.js')}}"></script>
  <script src="{{ url('front/lib/knob/jquery.knob.js')}}"></script>
  <script src="{{ url('front/lib/wow/wow.min.js')}}"></script>
  <script src="{{ url('front/lib/parallax/parallax.js')}}"></script>
  <script src="{{ url('front/lib/easing/easing.min.js')}}"></script>
  <script src="{{ url('front/lib/nivo-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
  <script src="{{ url('front/lib/appear/jquery.appear.js')}}"></script>
  <script src="{{ url('front/lib/isotope/isotope.pkgd.min.js')}}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="{{ url('front/contactform/contactform.js')}}"></script>

  <script src="{{ url('front/js/main.js')}}"></script>
</body>

</html>
